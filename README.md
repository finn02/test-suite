**NOTE**:  Currently only support Linux

### Installation  
* Run install.sh  

    ./install.sh

    
* Install cpgen package from pypi   

    pip install cpgen  
    
    
### Doc  

* Brute force file(**bf**) takes three agrument **your solution** , **brute force solution**, **input file**  

    bf sol.cpp brute.cpp test.py