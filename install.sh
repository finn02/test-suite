#!/bin/bash

function red    { echo -e "\033[31;1m$@\033[0;m" ; }
function Red    { echo -e "\033[31;5m$@\033[0;m" ; }
function green  { echo -e "\033[32;1m$@\033[0;m" ; }
function Green  { echo -e "\033[32;5m$@\033[0;m" ; }

green "Creating scripts folder"
mkdir -p ~/scripts

green "Adding scripts folder to PATH variable"
echo 'export PATH=$HOME/scripts:$PATH' >> ~/.bashrc

green "Copying files to scripts folder"
cp -v {bf,check.py} ~/scripts/
