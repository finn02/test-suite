#!/usr/bin/python
import sys

file1 = sys.argv[1]
file2 = sys.argv[2]

s = open(file1, 'r').read().split()
t = open(file2, 'r').read().split()

print(s == t)
