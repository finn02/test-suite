#!/bin/bash

function red    { echo -e "\033[31;1m$@\033[0;m" ; }
function Red    { echo -e "\033[31;5m$@\033[0;m" ; }
function green  { echo -e "\033[32;1m$@\033[0;m" ; }
function Green  { echo -e "\033[32;5m$@\033[0;m" ; }

pythonx=`which python3`
testcase=0
fail="False"
g="g++ -O2 -std=c++14 -Wall -Wextra -pedantic -Wformat=2 -Wfloat-equal -Wlogical-op -Wredundant-decls -Wconversion -Wcast-qual -Wcast-align     -Wuseless-cast -Wno-shadow -Wno-unused-result -Wno-unused-parameter -Wno-unused-local-typedefs -Wno-long-long -g -fsanitize=address,undefined"

FILE1=$1
FILE2=$2
FILE3=$3
Red "Compiling " ${FILE1}
${g} ${FILE1} -o A
Red "Compiling " ${FILE2}
${g} ${FILE2} -o BRUTE

Green "Enter number of test-cases : "
read -r no
while [ ${testcase} -lt ${no} ]
do

	testcase=`expr ${testcase} + 1`
	${pythonx} ${FILE3} > testcase.in
	./A < testcase.in > A.out
	./BRUTE < testcase.in > B.out
	result=$(${pythonx} ~/scripts/check.py A.out B.out)

	if [ "$result" == "$fail" ]
	then
		red "Wrong Answer! at ${testcase}:"
		cat testcase.in
		green "=> Correct Output : "
		cat B.out
		red "=> Incorrect Output : "
	    cat A.out
		break
	fi
   echo -en "\e[32;1m" $testcase "\e[0m"
done

# echo -en "TestCase : " $testcase " = " $result
#echo -e "\033[31;1m* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\033[0;m"
